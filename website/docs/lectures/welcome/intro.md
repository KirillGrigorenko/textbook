---
id: lecture-welcome-intro
slug: /lectures/welcome
sidebar_position: 1
description: В данном разделе содержатся описания проектно-технологической практики.
--- 

# 1. Цели и задачи дисциплины

## План

- Обзор разделов дисциплины
- Особенности дисциплины

## Материал

- [Презентация](https://gitlab.com/iu5edu/cpp-course-sem2/textbook/-/blob/main/src/lectures/welcome/presentation.pdf)

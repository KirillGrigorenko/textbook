---
id: lab2-UsefulInformation-intro
slug: /labs/lab2/UsefulInformation/
sidebar_position: 1
---

# Полезная информация

## Добавление текстового в файла в папку собранных исполняемых файлов при использовании CMake

Предположим, что вы в проекте лабораторной работы в директории `lab2` хотите расположить файл `source.txt`. Данный файл при сборке проекта вы хотите получить в той же папке, что и исполняемый файл в папке собранных исполняемых файлов. Иными словами, вы хотите данный файл увидеть вместе с исполняемым файлом. Чтобы CMake собирал проект необходимым нам образом, используйте команду [file](https://cmake.org/cmake/help/latest/command/file.html) с первым атрибутом `COPY`, как показано в примере ниже:

```cmake
cmake_minimum_required(VERSION 3.23)
project(lab2)

set(CMAKE_CXX_STANDARD 17)

// highlight-start
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/source.txt
        DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
// highlight-end

add_executable(lab2 main.cpp)
```

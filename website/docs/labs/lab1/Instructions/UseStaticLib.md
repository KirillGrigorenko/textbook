---
sidebar_position: 4
---

# Использование статической библиотеки

## Обновление `CMakeLists.txt` главного проекта

:::info
При подготовке материала использовалась статья [Полное руководство по CMake. Часть вторая: Система сборки](https://itnan.ru/post.php?c=1&p=432096).
:::

Давайте теперь сообщим CMake о том, что у нас в директории содержится статическая библиотека. Для этого изменим `CMakeLists.txt` главного проекта (лежащий в корне директории `lab1`) следующим образом:

```cmake
cmake_minimum_required(VERSION 3.23)

set(project "lab1")
project(${project})

set(CMAKE_CXX_STANDARD 17)

set(${project}_SOURCES
        main.cpp)

// highlight-start
add_subdirectory(mymath)
// highlight-end

set(${project}_SOURCE_LIST
        ${${project}_SOURCES})

add_executable(${project}
        ${${project}_SOURCE_LIST})

// highlight-start
target_link_libraries(${project} mymath)
// highlight-end
```

Тут мы чутка навернули переменных окружения, но смотрим на два важных изменения:

- команда [add_subdirectory](https://cmake.org/cmake/help/latest/command/add_subdirectory.html) побуждает CMake к незамедлительной обработке указанного файла подпроекта. Так как в `CMakeLists.txt` нашего подпроекта происходит подготовка статической библиотеки, то в основном проекте мы можем это использовать.
- [target_link_libraries](https://cmake.org/cmake/help/latest/command/target_link_libraries.html) компонует библиотеку или исполняемый файл с другими предоставляемыми библиотеками. Первым аргументом данная команда принимает название цели, сгенерированной с помощью команд `add_executable` или `add_library`, а последующие аргументы представляют собой названия целей библиотек или полные пути к библиотекам.

Таким образом при сборке основной проект будет знать о других библиотеках.

## Изменения `main.cpp`

Для демонстрации получившегося результата изменим файл `main.cpp` следующим образом:

```cpp
#include <iostream>
#include "mymath/mymath.h"

int main() {
    setlocale(LC_ALL, "Russian");
    std::cout << "Я умею суммировать! Зацени 1+1=";
    std::cout << mymath::sum(10, 32) << std::endl;

    return 0;
}
```

## А как отлаживать получившийся проект?

А об этом смотрите на электронном курсе предыдущей дисциплины:

- про [Visual Studio 2022 и CMake](https://cpp1.docs.iu5edu.ru/docs/labs/lab1/AutomatedLabs/solution/VSCMake);
- про [CLion и CMake](https://cpp1.docs.iu5edu.ru/docs/labs/lab1/AutomatedLabs/solution/CLion).

А вообще смотрите официальную доку:

- [CMake projects in Visual Studio](https://learn.microsoft.com/en-us/cpp/build/cmake-projects-in-visual-studio)
- [Quick CMake tutorial (CLion)](https://www.jetbrains.com/help/clion/quick-cmake-tutorial.html)
- [Get started with CMake Tools on Linux (Visual Studio Code)](https://code.visualstudio.com/docs/cpp/cmake-linux)

## А что дальше?

Создайте другую статическую библиотеку с другим крутым и полезным функционалом и добавьте его в основной проект. Обязательно в исполняемом проекте продемонстрируйте одновременное использование двух статических библиотек.

![Мем](images/happy-end-mem.gif).

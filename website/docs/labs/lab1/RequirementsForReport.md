---
sidebar_position: 5
---

# Требования к отчету

:::caution
На момент подготовки лабораторной работы еще не было принято решение о необходимости предоставления отчетов по результатам выполнения заданий лабораторных работ.
:::
